#!/bin/bash
# FIRST ARG = clickhouse HOST
# SECOND ARG = Native backup folder

TODAY=$(date +%Y-%m-%d)
YESTERDAY=$(date -d "yesterday 13:00" '+%Y-%m-%d')
HOST=$1
DUMP_FOLDER=$2
TODAY_RAW_FILE="$DUMP_FOLDER/oblivki-$TODAY.native"
YESTERDAY_RAW_FILE="$DUMP_FOLDER/oblivki-$YESTERDAY.native"

# little check
if [ -z "$1" ]; then
    echo "First argument must be clickhouse host!"
    exit 0
fi
if [ -z "$2" ]; then
    echo "Second argument must be clickhouse native dumps folder!"
    exit 0
fi

# Сделать native dump
clickhouse-client -h $HOST --query="SELECT * FROM oblivki.views WHERE date='$TODAY' FORMAT Native" > $TODAY_RAW_FILE

# Запаковать вчерашний + удалить
if [ -f $YESTERDAY_RAW_FILE ]; then
    # вчреашний файл будет неполныйм, сделаем норм бэкап
    clickhouse-client -h $HOST --query="SELECT * FROM oblivki.views WHERE date='$YESTERDAY' FORMAT Native" > $YESTERDAY_RAW_FILE
    zip "$DUMP_FOLDER/oblivki-$YESTERDAY" $YESTERDAY_RAW_FILE
    rm -f $YESTERDAY_RAW_FILE
fi
